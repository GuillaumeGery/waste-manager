Contexte du projet

Présentation

Afin d'aider les communes à améliorer leur gestion des déchets, vous devez développer une application qui aura pour fonction d'assigner les différents déchets générés sur la commune aux services de traitement des déchets disponibles.

​

Plusieurs types de déchets existent pour le moment, mais il est probable que d'autres se rajoutent par la suite (la conception devra le prendre en compte) :

    déchets gris, le déchet de base
    plastiques, qui seront de différents sous-types
    papiers/cartons
    déchets organiques
    verres
    métaux

​

Plusieurs services de traitements des déchets seront disponibles (de même, d'autres peuvent exister par la suite) :

    incinérateur, qui accepte tout type de déchets
    centre de recyclage, peut traiter un certain type de déchets, potentiellement selon leur sous-type
    composteur de quartier, peut traiter les déchets compostables

Chacun de ces services de traitement a une capacité de traitement définie en tonnes et rejette du CO2 relatif au nombre de déchets traités.

​

Objectifs

En utilisant le langage objet que vous souhaitez, vous allez devoir créer un programme qui acceptera en entrée 2 fichiers json (l'un contenant les déchets ainsi que les services de traitement des déchets disponibles, l'autre contenant les quantités de CO2 rejetées) et qui affichera le résultat de la répartition des déchets ainsi que le CO2 rejeté (par chaque service de traitement et au global).

​

L'idéal est de favoriser les traitements adaptés et de ne se replier sur les autres méthodes de traitement que lorsque les capacités maximales d'un service sont atteintes.

​

Vous allez devoir utiliser les différents principes de la POO (SOLID) et ses outils (classes, héritage, interfaces, abstraction, etc.) pour réaliser ce programme.

​

Bonus

Si vous souhaitez aller plus loin, voici quelques idées de fonctionnalités supplémentaires :

    ajouter un nouveau type de déchet
    ajouter un nouveau type de service de traitement
    pouvoir utiliser d'autres formats de données en entrée (xml, yaml…)
    ajouter différents statistiques en sortie (par exemple par quartier, par habitants…)
    faire une interface graphique avancée (choix des fichiers sources, saisie des données, graphiques…)

