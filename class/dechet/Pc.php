<?php 
require_once('Plastic.php');
require_once('class/interface/CalculQuantity.php');
require_once('class/interface/IncineratorInterface.php');

class Pc extends Plastic implements CalculQuantity, IncineratorInterface{
    protected int $amountPc;

    public function getVolume($type): int
    {
        $jsonData = 'json/data.json';

        $dataPc = json_decode(file_get_contents($jsonData),true);
        
        $pc = $dataPc['quartiers'];
        
        $total[]= '';
        foreach($pc as $value){
            array_push($total, (int)$value[$type]);
          //  print_r($total);
        }

        $totalCapacity = array_sum($total);
        $this->amountPc = (json_encode($totalCapacity));
        return $this -> amountPc;
    }
}