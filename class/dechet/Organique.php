<?php

require_once('class/interface/CalculQuantity.php');
require_once('class/interface/IncineratorInterface.php');
require_once('class/interface/CompostInterface.php');
require_once('class/dechet/Waste.php');

class Organic extends Waste implements CalculQuantity, IncineratorInterface, CompostInterface{
    protected int $amountOrganic;
    
    public function getVolume($type): int
    {
        $jsonData = 'json/data.json';

        $dataOrganic = json_decode(file_get_contents($jsonData),true);
        
        $organic = $dataOrganic['quartiers'];
        
        $total[]= '';
        foreach($organic as $value){
            array_push($total, (int)$value[$type]);
          //  print_r($total);
        }

        $totalCapacity = array_sum($total);
        $this->amountOrganic = (json_encode($totalCapacity));
        return $this -> amountOrganic;
    }

}
