<?php
require_once('Waste.php');
require_once('class/interface/Incinerator.php');
require_once('class/interface/CalculQuantity.php');

class Glass extends Waste implements CalculQuantity ,IncineratorInterface{
    protected int $amountGlass;
    
    public function getVolume($type): int
    {
        $jsonData = 'json/data.json';

        $dataGlass = json_decode(file_get_contents($jsonData),true);
        
        $glass = $dataGlass['quartiers'];
        
        $total[]= '';
        foreach($glass as $value){
            array_push($total, (int)$value[$type]);
          //  print_r($total);
        }

        $totalCapacity = array_sum($total);
        $this->amountGlass = (json_encode($totalCapacity));
        return $this -> amountGlass;
    }
    
}