<?php 
require_once('class/interface/CalculQuantity.php');
require_once('class/interface/IncineratorInterface.php');
require_once('class/dechet/Waste.php');

class Other extends Waste implements CalculQuantity, IncineratorInterface {
    protected int $amountOther;

    public function getVolume($type): int
    {
        $jsonData = 'json/data.json';

        $dataOther = json_decode(file_get_contents($jsonData),true);
        
        $other = $dataOther['quartiers'];
        
        $total[]= '';
        foreach($other as $value){
            array_push($total, (int)$value[$type]);
          //  print_r($total);
        }

        $totalCapacity = array_sum($total);
        $this->amountOther = (json_encode($totalCapacity));
        return $this -> amountOther;
    }
}