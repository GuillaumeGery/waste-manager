<?php 
require_once('class/interface/CalculQuantity.php');
require_once('class/interface/IncineratorInterface.php');
require_once('class/dechet/Waste.php');

class Paper extends Waste implements CalculQuantity, IncineratorInterface{
    protected int $amountPaper;

   public function getVolume($type): int
    {
        $jsonData = 'json/data.json';

        $dataPaper = json_decode(file_get_contents($jsonData),true);
        
        $paper = $dataPaper['quartiers'];
        
        $total[]= '';
        foreach($paper as $value){
            array_push($total, (int)$value[$type]);
          //  print_r($total);
        }

        $totalCapacity = array_sum($total);
        $this->amountPaper = (json_encode($totalCapacity));
        return $this -> amountPaper;
    }
}