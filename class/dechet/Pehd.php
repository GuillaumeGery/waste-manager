<?php 
require_once('Plastic.php');
require_once('class/interface/IncineratorInterface.php');
require_once('class/interface/CalculQuantity.php');

class Pehd extends Plastic implements CalculQuantity, IncineratorInterface{
    protected int $amountPehd;

    public function getVolume($type): int
    {
        $jsonData = 'json/data.json';

        $dataPehd = json_decode(file_get_contents($jsonData),true);
        
        $pehd = $dataPehd['quartiers'];
        
        $total[]= '';
        foreach($pehd as $value){
            array_push($total, (int)$value[$type]);
          //  print_r($total);
        }

        $totalCapacity = array_sum($total);
        $this->amountPehd = (json_encode($totalCapacity));
        return $this ->amountPehd;
    }
}