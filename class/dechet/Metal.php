<?php
require_once('class/interface/CalculQuantity.php');
require_once('class/interface/IncineratorInterface.php');
require_once('class/interface/MetalRecyclingInterface.php');
require_once('class/dechet/Waste.php');

class Metal extends Waste implements CalculQuantity, IncineratorInterface, MetalRecyclingInterface{
    protected int $amountMetal;
    
    public function getVolume($type): int
    {
        $jsonData = 'json/data.json';

        $dataMetal = json_decode(file_get_contents($jsonData),true);
        
        $metal = $dataMetal['quartiers'];
        
        $total[]= '';
        foreach($metal as $value){
            array_push($total, (int)$value[$type]);
          //  print_r($total);
        }

        $totalCapacity = array_sum($total);
        $this->amountMetal = (json_encode($totalCapacity));
        return $this -> amountMetal;
    }
   
    
}