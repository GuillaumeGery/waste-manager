<?php 
require_once('Plastic.php');
require_once('class/interface/IncineratorInterface.php');
require_once('class/interface/CalculQuantity.php');

class Pvc extends Plastic implements CalculQuantity, IncineratorInterface{
    protected int $amountPvc;

    public function getVolume($type): int
    {
        $jsonData = 'json/data.json';

        $dataPvc = json_decode(file_get_contents($jsonData),true);
        
        $pvc = $dataPvc['quartiers'];
        
        $total[]= '';
        foreach($pvc as $value){
            array_push($total, (int)$value[$type]);
          //  print_r($total);
        }

        $totalCapacity = array_sum($total);
        $this->amountPvc= (json_encode($totalCapacity));
        return $this -> amountPvc;
    }
}