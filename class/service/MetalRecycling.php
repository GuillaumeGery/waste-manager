<?php
require_once('Service.php');

class MetalRecycling extends Service{
    protected int $amountMax;
    protected int $co2Amount;

   /* public function __construct(int $amountMax, int $co2Amount )
    {
        $this->amountMax = $amountMax;
        $this->co2Amount = $co2Amount;
        
    }*/

    public function setWaste(Waste $waste): self
    {
        if(!($waste instanceof MetalRecyclingInterface)){
            throw new Exception('Is not waste for metal recycling');
        }
        return parent::setWaste($waste);
    }
}