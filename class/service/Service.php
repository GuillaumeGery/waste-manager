<?php 
require_once('class/dechet/Waste.php');

class Service{
    private Waste $waste;

    public function setWaste( Waste $waste):self
    {
        $this->waste = $waste;
        return $this;
    }

    public function getWaste()
    {
        return $this->waste;
    }
}