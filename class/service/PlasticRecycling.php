<?php 
require_once('Service.php');

class PlasticRecycling extends Service{
    protected int $maxAmount;
    protected int $co2Amount;

    /*public function __construct(int $maxAmount, int $co2Amount)
    {
        $this->maxAmount = $maxAmount;
        $this->co2Amount = $co2Amount;
    }*/

    public function setWaste(Waste $waste): self
    {
        if(!($waste instanceof IncineratorInterface)){
            throw new Exception('Is not waste for incinerator');
        }
        return parent::setWaste($waste);
    }
}