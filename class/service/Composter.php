<?php
require_once('Service.php');
require_once('class/interface/IncineratorInterface.php');

class Composter extends Service {
    protected int $maxAmount;
    protected int $co2Amount;

    public function __construct(int $maxAmount, int $co2Amount)
    {
        $this->maxAmount = $maxAmount;
        $this->co2Amount = $co2Amount;
        
    }

    public function setWaste(Waste $waste): self
    {
        if(!($waste instanceof CompostInterface)){
            throw new Exception('Is not waste for composter');
        }
        return parent::setWaste($waste);
    }
     
}